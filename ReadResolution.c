#include <windows.h>
#include <stdlib.h>

typedef struct
{
    int rows, columns;
} table;
// rows = baris, columns = kolom

//-------------------------------- Algoritm --------------------------------
table ReadResolution()
{
    table Result;

    CONSOLE_SCREEN_BUFFER_INFO csbi;
    GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
    Result.columns = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    Result.rows = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;

    return Result;
}
//-------------------------------- Algoritm --------------------------------

void printc(char Pesan[]);
void CenterVertical(int line);

int main()
{
    table Resolution;
    char Temp1[100];
    char Temp2[100];
    char TempNum1[10];
    char TempNum2[10];

start:
    system("cls");
    Resolution = ReadResolution();

    strcpy(Temp1, "Max columns : ");
    strcpy(Temp2, "Max row : ");
    itoa(Resolution.columns, TempNum1, 10);
    itoa(Resolution.rows, TempNum2, 10);
    strcat(Temp1, TempNum1);
    strcat(Temp2, TempNum2);

    CenterVertical(2);
    printc(Temp1);
    printf("\n");
    printc(Temp2);

    Sleep(500);

    goto start;

    return 0;
}

void printc(char Pesan[])
{
    int columns = ReadResolution().columns;
    if (((columns - strlen(Pesan)) / 2) > 1)
    {
        for (size_t i = 0; i < ((columns - strlen(Pesan)) / 2); i++)
        {
            printf(" ");
        }
    }
    // ^ Printf spasi sebanyak yang dibutuhkan teks agar memiliki posisi tengah

    printf("%s", Pesan);
    // ^ Print pesan yang ingin diletakan di tengah layar
}

void CenterVertical(int line)
{
    for (size_t i = 0; i < ((ReadResolution().rows / 2) - line); i++)
    {
        printf("\n");
    }
}